# brought to you by anon and the vim-gangc
import requests

def hello_http(request):
	request_json = request.get_json(silent=True)
	request_args = request.args

	def get_article(url):
		headers = {'user-agent':'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'}
		r = requests.get(url, headers=headers)
		print('Requested website at {}\t got status code {}'.format(url, r.status_code))
		return r.text

	if request_json and 'url' in request_json:
		url = request_json['url']
		html = get_article(url)
	elif request_args and 'url' in request_args:
		url = request_args['url']
		html = get_article(url)
	else:
		html = '<h1>404 no "url" in request</h1>'

	return html
